import argparse
import csv
import fileinput
import pathlib
import re

import yaml


def arguments():
    parser = argparse.ArgumentParser(description="Generate reports about Citrix ADC WAF configuration")
    parser.add_argument('--waf', dest="waf", default='WAF.yml', type=open, help="File with WAF configurations things")
    parser.add_argument('--signatures', dest='sign', type=pathlib.Path, help="Path to the files with signatures")
    parser.add_argument('nsconf', type=open, help="WAF configuration file, ns.conf")
    parser.add_argument('--profile', dest="profile", type=pathlib.Path, help="Output CSV file for appFW settings")
    parser.add_argument('--sreport', dest="sreport", type=pathlib.Path, help="Output CSV file for signatures settings")
    __args__ = parser.parse_args()
    return __args__


def sec_checks(pars):
    profile_search = "add appfw profile"
    regexp = r"add appfw profile ([\w-]+)"
    waf = yaml.safe_load(pars.waf)['WAF']['check']
    with open(pars.profile, "w", newline='') as csv_file:
        csv_out = csv.writer(csv_file)
        table = ['profile', 'signatures'] + list(waf)
        csv_out.writerow(table)
        for line in pars.nsconf:
            if not re.search(profile_search, line):
                continue
            trow = [re.search(regexp, line).group(1)]\
                + [re.search("-"+table[1]+r"\s+([\w-]+)", line).group(1)]\
                + [waf[m]['Default'] if "-"+m not in line else re.search("-"+m+r"\s+([\w\s]+)(\s-)?", line).group(1) for m in table[2:]]
            csv_out.writerow(trow)


def signatures(pars):
    rep_list = list()
    rep_dict = dict()
    with fileinput.input(files=pars.sign.iterdir()) as f:
        for line in f:
            if "<SignaturesFile" in line:
                re_version = re.search(r'schema_version="(\d+)"\s+version="(\d+)"\s+\w+="(\d+)"', line)
                rep_dict = {
                    "Name": f.filename().name,
                    "version": re_version.group(1)+"."+re_version.group(2)+"."+re_version.group(3),
                    "enabled": 0,
                    "block": 0,
                    "log": 0,
                    "stats": 0}
                rep_list.append(rep_dict)
            if "<SignatureRule" in line:
                actions = re.search(r'actions=\"([\w,]+)"', line)
                rep_dict["enabled"] = rep_dict["enabled"] + 1 \
                    if re.search(r'enabled="(ON|OFF)"', line).group(1) == "ON" else rep_dict["enabled"]
                rep_dict["block"] = rep_dict["block"] + 1 \
                    if "block" in re.search(r'actions="([\w,]*)"', line).group(1) else rep_dict["block"]
                rep_dict["log"] = rep_dict["log"] + 1 \
                    if "log" in re.search(r'actions="([\w,]*)"', line).group(1) else rep_dict["log"]
                rep_dict["stats"] = rep_dict["stats"] + 1 \
                    if "stats" in re.search(r'actions="([\w,]*)"', line).group(1) else rep_dict["stats"]
    with open(pars.sreport, "w", newline='') as csv_file:
        csv_out = csv.DictWriter(csv_file, fieldnames=list(rep_list[0]))
        csv_out.writeheader()
        csv_out.writerows(rep_list)
    print(rep_list)


if __name__ == '__main__':
    parameters = arguments()
    print(parameters)
    if parameters.profile is not None:
        sec_checks(parameters)
    if parameters.sign is not None and parameters.sreport is not None:
        signatures(parameters)
